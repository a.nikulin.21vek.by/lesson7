export class Treasure {
  private _name: string = 'Rubin';
  private _cost: number = 200;
  constructor(cost: number, name: string) {
    if (cost) {
      this._cost = cost;
    }
    if (name) {
      this._name = name;
    }
  }
  public get name() {
    return this._name;
  }

  public get cost() {
    return this._cost;
  }
  public vyhod() {
    console.log('Камень', this._name, 'Стоимость равна', this._cost);
  }
  public get info() {
    `${this.name}|${this.cost}`; //то же что и на 24стр
    return this.name + '|' + this.cost;
  }
}
//get для получения данных, обращение функции как к полю
