import { Treasure } from './treasure';

export class Cave {
  treasures: Treasure[] = [];
  constructor() {
    const treasure1 = new Treasure(400, 'rubin');
    const treasure2 = new Treasure(300, 'izumrud');
    const treasure3 = new Treasure(1000, 'almaz');
    this.treasures.push(treasure1, treasure2, treasure3);
  }

  cost() {
    console.log(
      'Соковища отсортированны :',
      this.treasures.sort(function (tr1, tr2) {
        return tr2.cost - tr1.cost;
      }),
    );
  }
  list() {
    this.treasures.forEach(function (treasure) {
      console.log(treasure.info);
    });
  }
}
