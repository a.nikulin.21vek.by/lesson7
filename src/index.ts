const btn1 = document.querySelector('#btn1'); // достаем кнопки из хтмл
const btn2 = document.querySelector('#btn2');
const body: any = document.getElementsByTagName('body')[0];
function changeColor1() {
  body.style.background = 'fuchsia';
}
function changeColor2() {
  body.style.background = 'orange';
}
btn1?.addEventListener('click', changeColor1);
btn2?.addEventListener('click', changeColor2);
